package kmsg.sms.teacher.model;

public class TeacherDocModel {

	private int teacherId;
	private int docTypeId;
	private String docPath;
	
	public int getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(int teacherId) {
		this.teacherId = teacherId;
	}
	
	public int getDocTypeId() {
		return docTypeId;
	}
	public void setDocTypeId(int docTypeId) {
		this.docTypeId = docTypeId;
	}
	public String getDocPath() {
		return docPath;
	}
	public void setDocPath(String docPath) {
		this.docPath = docPath;
	}
	public TeacherDocModel() {

	}

	public TeacherDocModel(int teacherId, int docTypeId, String docPath) {
		super();
		this.teacherId = teacherId;
		this.docTypeId = docTypeId;
		this.docPath = docPath;
	}
	

	public TeacherDocModel(int schoolId, int teacherId, int docTypeId, String docPath) {
		super();
		this.teacherId = teacherId;
		this.docTypeId = docTypeId;
		this.docPath = docPath;
	}

	
	
	
}
