package kmsg.sms.mst.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import kmsg.sms.mst.daoint.ManageClassDaoInt;
import kmsg.sms.mst.model.ManageClass;
import kmsg.sms.common.SMSLogger;
import kmsg.sms.common.SvcStatus;

@Repository
public class ManageClassDaoImpl implements ManageClassDaoInt,SMSLogger {
	
	@Autowired
	JdbcTemplate template;	

	@Override
	public Map<String, Object> updateClass(ManageClass model) {
		
		// TODO Auto-generated method stub

		final String SQL = "UPDATE class_section SET class_id = ? ,"
				+ " section = ? ,"
				+ "building_id = ?  ,"
				+ "room_id = ? ,"
				+ "max_students = ?"
				+ " WHERE section_id = ?";

	    int count = 0;
		try {
			 count = template.update (SQL, new Object[]
				 {
					 model.getClassId(),
					 model.getSection(),
					 model.getBuildingId(),
					 model.getRoomId(),
					 model.getMaxStudents(),
					 model.getSectionId()
				} );
		}
		catch(DuplicateKeyException e) {
			logger.error("updateSection : Duplicate Key for this Section:" + model.getSection());
			return SvcStatus.GET_FAILURE("Section already exist for this class");
		}
		catch(Exception e) {
			logger.error("updateSection :Exception occured in updateSection:" + model.getSection() + ": " + e.getMessage());
			return SvcStatus.GET_FAILURE("Error occured in updating Section. Contact System Admin");
		}
		
		if (count > 0 ) {
			Map<String, Object> data = new HashMap<>();
			data.put(SvcStatus.STATUS, SvcStatus.SUCCESS);
			data.put(SvcStatus.MSG, "Class Section Updated");
			return data;
		}
		else {
			logger.error("updateSection : Failed to update Section :" +  model.getSection());
			return SvcStatus.GET_FAILURE("Section could not be updated. Contact System Admin");
		}

	}
	

	@Override
	public Map<String, Object> saveClass(ManageClass model) {
		final String SQL = "INSERT INTO class_section (class_id,section,building_id,room_id,max_students) VALUES (?,?,?,?,?)";

	    int count = 0;
		KeyHolder holder = new GeneratedKeyHolder();
		try {
			 count = template.update (
				new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(
													SQL,
													Statement.RETURN_GENERATED_KEYS
																	);
						ps.setInt(1, model.getClassId());
						ps.setString(2, model.getSection());
						ps.setInt(3, model.getBuildingId());
						ps.setInt(4, model.getRoomId());
						ps.setInt(5, model.getMaxStudents());
						
						return ps ;
					}
				}, holder ) ;
		}
		catch(DuplicateKeyException e) {
			logger.error("insertSection : Duplicate Key insertSection:" + model.getSectionId());
			return SvcStatus.GET_FAILURE("Section already exist for this class");
		}
		catch(Exception e) {
			e.printStackTrace();
			logger.error("insertSection :Exception occured in insertSection:" + model.getSectionId() + ": " + e.getMessage());
			return SvcStatus.GET_FAILURE("Error occured in adding Section. Contact System Admin");
		}
		
		if (count > 0 ) {
			Map<String, Object> data = new HashMap<>();
			data.put(SvcStatus.MSG, "Class Section Added");
			data.put(SvcStatus.STATUS, SvcStatus.SUCCESS);
			return data;
		}
		else {
			return SvcStatus.GET_FAILURE("Section could not be added. Contact System Admin");
		}
	}

}
