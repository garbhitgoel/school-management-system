package kmsg.sms.mst.model;

public class Classes 
{
	private int classId;
	private String classLabel;
	
	public int getClassId() {
		return classId;
	}
	public void setClassId(int classId) {
		this.classId = classId;
	}
	public String getClassLabel() {
		return classLabel;
	}
	public void setClassLabel(String classLabel) {
		this.classLabel = classLabel;
	}
	public Classes(int classId, String classLabel) {
		this.classId = classId;
		this.classLabel = classLabel;
	}
	public Classes() {
	}
}
