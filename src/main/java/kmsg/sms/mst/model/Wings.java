package kmsg.sms.mst.model;

public class Wings 
{
	private int wingId;
	private String wing;
	private int fromClassId;
	private int toClassId;
	private String fromClass;
	private String toClass;
	
	public int getWingId() {
		return wingId;
	}
	public void setWingId(int wingId) {
		this.wingId = wingId;
	}
	public String getWing() {
		return wing;
	}
	public void setWing(String wing) {
		this.wing = wing;
	}
	public int getFromClassId() {
		return fromClassId;
	}
	public void setFromClassId(int fromClassId) {
		this.fromClassId = fromClassId;
	}
	public int getToClassId() {
		return toClassId;
	}
	public void setToClassId(int toClassId) {
		this.toClassId = toClassId;
	}
	public String getFromClass() {
		return fromClass;
	}
	public void setFromClass(String fromClass) {
		this.fromClass = fromClass;
	}
	public String getToClass() {
		return toClass;
	}
	public void setToClass(String toClass) {
		this.toClass = toClass;
	}
}
