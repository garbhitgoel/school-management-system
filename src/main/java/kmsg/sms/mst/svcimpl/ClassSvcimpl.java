package kmsg.sms.mst.svcimpl;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kmsg.sms.common.SvcStatus;
import kmsg.sms.common.session.AdminSessionValidator;
import kmsg.sms.mst.adapter.ClassAdapter;
import kmsg.sms.mst.svcint.ClassSvcInt;

@RestController
@RequestMapping("/sms/mst/class")
public class ClassSvcimpl implements ClassSvcInt
{
	@Autowired
	ClassAdapter adapter;
	
	@Autowired
	AdminSessionValidator validator;
	
	@RequestMapping(value="/list", method = RequestMethod.POST, headers="Accept=application/json")
	public Map<String, Object> getClassList(@RequestParam Map<String, String> params, HttpSession httpSession,HttpServletRequest request, HttpServletResponse response) 
	{
		Map<String,Object> map = new HashMap<>();
		String CurrMethod = new Throwable().getStackTrace()[0].getMethodName();

		map = validator.validateAdminSession(httpSession, request, CurrMethod);
		
		if(map.get(SvcStatus.STATUS).equals(SvcStatus.SUCCESS))
		{	
			return adapter.getClassList();
		}
		return map;
	}
}
