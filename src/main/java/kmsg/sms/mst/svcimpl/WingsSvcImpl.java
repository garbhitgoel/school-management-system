package kmsg.sms.mst.svcimpl;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kmsg.sms.common.SMSLogger;
import kmsg.sms.common.SvcStatus;
import kmsg.sms.common.session.SchoolSessionValidator;
import kmsg.sms.mst.adapter.WingsAdapter;
import kmsg.sms.mst.svcint.WingsSvcInt;

@RestController
@RequestMapping("/sms/wings")
public class WingsSvcImpl implements WingsSvcInt, SMSLogger
{
	@Autowired
	WingsAdapter adapter;
	
	@Autowired
	SchoolSessionValidator validator;
	
	@Override
	@RequestMapping(value="/save", method = RequestMethod.POST, headers="Accept=application/json")
	public Map<String, Object> saveWing(@RequestParam Map<String, String> params, HttpSession httpSession,HttpServletRequest request, HttpServletResponse response) 
	{
		Map<String,Object> map = new HashMap<>();
		String CurrMethod = new Throwable().getStackTrace()[0].getMethodName();
		map = validator.validateSchoolSession(httpSession, request, CurrMethod);
		
		if(map.get(SvcStatus.STATUS).equals(SvcStatus.SUCCESS))
		{
			int schoolId=(int) map.get("schoolId");
			adapter.setSchoolId(schoolId);
			String wings = params.get("wings");
			
			return adapter.addNewWing(wings);
		}
		return map;
	}
}
