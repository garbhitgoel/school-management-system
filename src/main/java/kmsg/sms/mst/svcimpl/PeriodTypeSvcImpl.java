package kmsg.sms.mst.svcimpl;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kmsg.sms.mst.adapter.PeriodTypeAdapter;
import kmsg.sms.common.SvcStatus;
import kmsg.sms.common.session.AdminSessionValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kmsg.sms.mst.svcint.PeriodTypeSvcInt;

@RestController
@RequestMapping("/sms/a/type")
public class PeriodTypeSvcImpl implements PeriodTypeSvcInt {
	
	@Autowired
	PeriodTypeAdapter adapter;
	
	@Autowired
	AdminSessionValidator validator;
	
	@Override
	@RequestMapping(value="/list", method = RequestMethod.POST, headers="Accept=application/json")
	public Map<String, Object> TypeList(@RequestParam Map<String, String> params, HttpSession httpSession,HttpServletRequest request, HttpServletResponse response) 
	{
		Map<String,Object> map = new HashMap<>();
		String CurrMethod = new Throwable().getStackTrace()[0].getMethodName();

		map = validator.validateAdminSession(httpSession, request, CurrMethod);
		
		if(map.get(SvcStatus.STATUS).equals(SvcStatus.SUCCESS))
		{	
			System.out.println("inside typelist function");	
			return adapter.TypeList();
		}
		return map;
	}
	
	@Override
	@RequestMapping(value="/save", method = RequestMethod.POST, headers="Accept=application/json")
	public Map<String, Object> TypeSave(@RequestParam Map<String, String> params, HttpSession httpSession,HttpServletRequest request, HttpServletResponse response) 
	{
		Map<String,Object> map = new HashMap<>();
		String CurrMethod = new Throwable().getStackTrace()[0].getMethodName();
		map = validator.validateAdminSession(httpSession, request, CurrMethod);
		
		if(map.get(SvcStatus.STATUS).equals(SvcStatus.SUCCESS))
		{
			String project = params.get("periodType");
			return adapter.addPeriodType(project);
		}
		return map;
	}
}
