package kmsg.sms.mst.svcimpl;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kmsg.sms.mst.adapter.ManageClassAdapter;
import kmsg.sms.mst.svcint.ManageClassSvcInt;
import kmsg.sms.common.SvcStatus;
import kmsg.sms.common.session.AdminSessionValidator;

@RestController
@RequestMapping("/sms/a/section")
public class ManageClassSvcImpl implements ManageClassSvcInt
{
	@Autowired
	ManageClassAdapter adapter;
	
	@Autowired
	AdminSessionValidator validator;
	
	@Override
	@RequestMapping(value="/save", method = RequestMethod.POST, headers="Accept=application/json")
	public Map<String, Object> saveSection(@RequestParam Map<String, String> params, HttpSession httpSession,HttpServletRequest request, HttpServletResponse response) 
	{
		Map<String,Object> map = new HashMap<>();
		String CurrMethod = new Throwable().getStackTrace()[0].getMethodName();
		map = validator.validateAdminSession(httpSession, request, CurrMethod);
		
		if(map.get(SvcStatus.STATUS).equals(SvcStatus.SUCCESS))
		{
			String timing = params.get("section");
			return adapter.addSection(timing);
		}
		return map;
	}
}
