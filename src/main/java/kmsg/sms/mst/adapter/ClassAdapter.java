package kmsg.sms.mst.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import kmsg.sms.mst.daoimpl.ClassDaoImpl;

@Component
public class ClassAdapter {

	@Autowired
	ClassDaoImpl dao;
	
	public Map<String, Object> getClassList() 
	{
		return dao.getClassList();
	}

}
